# SPDX-FileCopyrightText: 2022 Benjamin Kahlau
#
# SPDX-License-Identifier: GPL-3.0-or-later
NPM := npm
BIN := ./node_modules/.bin
POSTCSS := $(BIN)/postcss
PRETTIER := $(BIN)/prettier
PRETTIER_FLAGS := --cache --cache-strategy metadata --write
STYLELINT := $(BIN)/stylelint
STYLELINT_FLAGS := --fix

SOURCES := $(filter-out source/roanapur.min.css, $(wildcard **/*.md **/*.ejs **/*.js **/*.css))

SRC_JS := $(filter %.js, $(SOURCES))
SRC_CSS := $(filter %.css, $(SOURCES))
SRC_MD := $(filter %.md, $(SOURCES))
SRC_EJS := $(filter %.ejs, $(SOURCES))

all: node_modules source/roanapur.min.css

node_modules: package.json
	$(NPM) prune && $(NPM) ci

source/roanapur.min.css: src/roanapur.css postcss.config.js tailwind.config.js $(SRC_EJS) ## Compile stylesheets
	$(POSTCSS) $< --output $@

format: $(SRC_EJS) $(SRC_JS) $(SRC_CSS) $(SRC_MD) ## Format code with prettier
	$(PRETTIER) $^ $(PRETTIER_FLAGS)

lint: eslint stylelint

eslint: eslint.config.js $(SRC_JS) ## Lint code with eslint
	$(ESLINT) --config $< $(filter-out $<, $^) $(ESLINT_FLAGS)

stylelint: stylelint.config.js $(SRC_CSS) ## Lint code with eslint
	$(STYLELINT) --config $< $(filter-out $<, $^) $(STYLELINT_FLAGS)

clean: ## Clean minified files
	$(RM) -rf source/roanapur.min.css
	$(RM) -rf source/roanapur.min.css.map

help: ## Show this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-10s\033[0m %s\n", $$1, $$2}'

.PHONY: help clean format lint eslint stylelint
