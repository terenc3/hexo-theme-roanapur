/*
 * SPDX-FileCopyrightText: 2022 Benjamin Kahlau
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
const iconify = require('@iconify/tailwind');

module.exports = {
    content: ['./layout/**/*.ejs'],
    safelist: [
        'float-right',
        'float-left',
        'float-none',
        {
            pattern: /flex-[\w+]/
        },
        {
            pattern: /font-[\w+]/
        },
        {
            pattern: /border-[\w+]-[\d+]/
        },
        {
            pattern: /text-[\d|\w+]/
        },
        {
            pattern: /w-[\d+]/
        },
        {
            pattern: /h-[^-+]/
        },
        {
            pattern: /p[lrtb]?-[\d+]/
        },
        {
            pattern: /m[lrtb]?-[\d+]/
        }
    ],
    theme: {
        extend: {}
    },
    variants: {
        extend: {}
    },
    plugins: [
        iconify.addDynamicIconSelectors()
    ]
}
