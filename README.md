<!--
SPDX-FileCopyrightText: 2022 Benjamin Kahlau

SPDX-License-Identifier: GPL-3.0-or-later
-->

# Roanapur Hexo Theme

> My website theme for hexo

See at https://roanapur.de

## Features

-   Top/Footer menu
-   Social links

## Todo

-   devicons directive
-   simple icons directive
-   <%- toc(page.content) %>

## Examples
```markdown
Please press <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>R</kbd> to re-render an MDN page.

You can use <abbr title="Cascading Style Sheets">CSS</abbr> to style your <abbr>HTML</abbr> (HyperText Markup Language).

<kbd><kbd>Ctrl</kbd>+<kbd>N</kbd></kbd>

<samp><kbd>git add my-new-file.cpp</kbd></samp>
<code>
    <samp><kbd>custom-git ad my-new-file.cpp</kbd></samp>
</code>

To create a new file, choose the <kbd><kbd><samp>File</samp></kbd><kbd><samp>New Document</samp></kbd></kbd> menu option.

<pre><code><samp><kbd>md5 -s "Hello world"</kbd>
MD5 ("Hello world") = 3e25960a79dbc69b674cd4ec67a72c62
<kbd>md5 -s "Hello world"</kbd>
MD5 ("Hello world") = 3e25960a79dbc69b674cd4ec67a72c62
</samp></pre></code>
```

