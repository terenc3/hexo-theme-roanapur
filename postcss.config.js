/*
 * SPDX-FileCopyrightText: 2022 Benjamin Kahlau
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

module.exports = {
    map: {
        inline: false
    },
    plugins: [
        require('postcss-import'),
        require('postcss-url')({
            url: 'inline'
        }),
        require('tailwindcss'),
        require('autoprefixer'),
        require('cssnano')({
            preset: 'default'
        })
    ]
}
